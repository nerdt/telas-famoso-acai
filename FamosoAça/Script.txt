﻿CREATE DATABASE FamosoAcaiDB;
USE FamosoAcaiDB;

CREATE TABLE `tb_funcionario` (
	`id_funcionario` int NOT NULL AUTO_INCREMENT,
	`nm_nome` varchar(50) NOT NULL,
	`ds_email` varchar(250) NOT NULL UNIQUE,
	`ds_cpf` varchar(50) NOT NULL UNIQUE,
	`ds_rg` varchar(50) NOT NULL UNIQUE,
    `ds_salario` decimal(15,2) NOT NULL,
	`dt_nascimento` varchar(10) NOT NULL,
	`ds_cidade` varchar(60) NOT NULL,
	`ds_estado` varchar(30) NOT NULL,
	`ds_cep` varchar(10) NOT NULL,
	`ds_telefone` varchar(20) NOT NULL,
	`ds_bairro` varchar(60) NOT NULL UNIQUE,
	`ds_rua` varchar(200) NOT NULL,
	`id_depto` int NOT NULL UNIQUE,
	`img_funcionario` longblob NOT NULL,
	PRIMARY KEY (`id_funcionario`)
);

CREATE TABLE `tb_depto` (
	`id_depto` int NOT NULL AUTO_INCREMENT,
	`nm_depto` varchar(100) NOT NULL,
	`ds_depto` varchar(200) NOT NULL,
	PRIMARY KEY (`id_depto`)
);

CREATE TABLE `tb_fornecedor` (
	`id_fornecedor` int NOT NULL AUTO_INCREMENT,
	`ds_cnpj` varchar(25) NOT NULL UNIQUE,
	`nm_fornecedor` varchar(150) NOT NULL,
	`ds_bairro` varchar(150) NOT NULL,
	`ds_cep` varchar(12) NOT NULL,
	`ds_cidade` varchar(60) NOT NULL,
	`ds_estado` varchar(30) NOT NULL,
	`ds_telefone` int(14) NOT NULL,
	`ds_email` varchar(120) NOT NULL UNIQUE,
	`img_logo` longblob NOT NULL,
	PRIMARY KEY (`id_fornecedor`)
);

CREATE TABLE `tb_produto` (
	`id_produto` int NOT NULL AUTO_INCREMENT,
	`nm_nome` varchar(150) NOT NULL,
	`vl_preco` DECIMAL(12,2) NOT NULL,
	`ds_descricao` varchar(255),
	PRIMARY KEY (`id_produto`)
);

CREATE TABLE `tb_login` (
	`id_usuario` int NOT NULL AUTO_INCREMENT,
	`nm_usuario` varchar(60) NOT NULL,
	`ds_senha` varchar(50) NOT NULL,
	`pr_permissaoAdm` bool NOT NULL,
	`pr_permissaoCadastro` bool NOT NULL,
	`pr_permissaoConsulta` bool NOT NULL,
	`pr_permissaoContabilidade` bool NOT NULL,
	PRIMARY KEY (`id_usuario`)
);

CREATE TABLE `tb_estoque` (
	`id_estoque` int NOT NULL AUTO_INCREMENT,
	`nm_produto` varchar(250) NOT NULL,
	`id_compra` int NOT NULL,
	`qtd_estocado` bigint(100) NOT NULL,
	`qtd_minima` int NOT NULL,
	PRIMARY KEY (`id_estoque`)
);

CREATE TABLE `tb_compra` (
	`id_compra` int NOT NULL AUTO_INCREMENT,
	`id_item` int NOT NULL,
	`id_fornecedor` int NOT NULL,
	`qtd_comprado` bigint(100) NOT NULL,
	`dt_compra` varchar(10) NOT NULL,
	`vl_preco` DECIMAL(15,2) NOT NULL,
	PRIMARY KEY (`id_compra`)
);

CREATE TABLE `tb_venda` (
	`id_venda` int NOT NULL AUTO_INCREMENT,
	`id_produto` int NOT NULL,
	`dt_data_venda` varchar(10) NOT NULL,
	`vl_total_venda` DECIMAL(15,2) NOT NULL,
	PRIMARY KEY (`id_venda`)
);

CREATE TABLE `tb_produto_venda` (
	`id_item_venda` int NOT NULL AUTO_INCREMENT,
	`id_produto` int NOT NULL,
	`id_venda` int NOT NULL,
	PRIMARY KEY (`id_item_venda`)
);

CREATE TABLE `tb_folha_pagamento` (
	`id_folha_pag` int NOT NULL AUTO_INCREMENT,
	`id_funcionario` int NOT NULL,
	`id_inss` int NOT NULL,
	`ds_horas_extras` varchar(100) NOT NULL,
	`int_faltas` int NOT NULL,
    `ds_atraso` varchar(10) NOT NULL,
	`id_imposto_renda` int NOT NULL,
	`vl_imposto_de_renda` DECIMAL(15,2) NOT NULL,
	`vl_fgts` DECIMAL(15,2) NOT NULL,
	`vl_vale_transporte` DECIMAL(15,2) NOT NULL,
	`vl_salario_liq` DECIMAL(15,2) NOT NULL,
	`id_sal_familia` int NOT NULL,
	PRIMARY KEY (`id_folha_pag`)
);

CREATE TABLE `tb_inss` (
	`id_inss` int NOT NULL AUTO_INCREMENT,
	`vl_aliquota` DECIMAL(4,2) NOT NULL,
	PRIMARY KEY (`id_inss`)
);

CREATE TABLE `tb_imposto_renda` (
	`id_imposto_renda` int NOT NULL,
	`id_folha_pag` int NOT NULL,
	`vl_aliquota` DECIMAL(15,2) NOT NULL,
	`vl_deducao` DECIMAL(15,2) NOT NULL,
	PRIMARY KEY (`id_imposto_renda`)
);

CREATE TABLE `tb_salario_familia` (
	`id_sal_familia` int NOT NULL AUTO_INCREMENT,
	`vl_salario_bruto` DECIMAL(15,2) NOT NULL,
	`vl_sal_familia` DECIMAL(15,2) NOT NULL,
	`int_qtd_dependentes` int(2),
	PRIMARY KEY (`id_sal_familia`)
);

CREATE TABLE `tb_item` (
	`id_item` int NOT NULL AUTO_INCREMENT,
	`nm_item` varchar(50) NOT NULL,
	`ds_item` varchar(200) NOT NULL,
	PRIMARY KEY (`id_item`)
);

ALTER TABLE `tb_funcionario` ADD CONSTRAINT `tb_funcionario_fk0` FOREIGN KEY (`id_depto`) REFERENCES `tb_depto`(`id_depto`);

ALTER TABLE `tb_estoque` ADD CONSTRAINT `tb_estoque_fk0` FOREIGN KEY (`id_compra`) REFERENCES `tb_compra`(`id_compra`);

ALTER TABLE `tb_compra` ADD CONSTRAINT `tb_compra_fk0` FOREIGN KEY (`id_item`) REFERENCES `tb_item`(`id_item`);

ALTER TABLE `tb_compra` ADD CONSTRAINT `tb_compra_fk1` FOREIGN KEY (`id_fornecedor`) REFERENCES `tb_fornecedor`(`id_fornecedor`);

ALTER TABLE `tb_venda` ADD CONSTRAINT `tb_venda_fk0` FOREIGN KEY (`id_produto`) REFERENCES `tb_produto`(`id_produto`);

ALTER TABLE `tb_produto_venda` ADD CONSTRAINT `tb_produto_venda_fk0` FOREIGN KEY (`id_produto`) REFERENCES `tb_produto`(`id_produto`);

ALTER TABLE `tb_produto_venda` ADD CONSTRAINT `tb_produto_venda_fk1` FOREIGN KEY (`id_venda`) REFERENCES `tb_venda`(`id_venda`);

ALTER TABLE `tb_folha_pagamento` ADD CONSTRAINT `tb_folha_pagamento_fk0` FOREIGN KEY (`id_funcionario`) REFERENCES `tb_funcionario`(`id_funcionario`);

ALTER TABLE `tb_folha_pagamento` ADD CONSTRAINT `tb_folha_pagamento_fk1` FOREIGN KEY (`id_inss`) REFERENCES `tb_inss`(`id_inss`);

ALTER TABLE `tb_folha_pagamento` ADD CONSTRAINT `tb_folha_pagamento_fk2` FOREIGN KEY (`id_imposto_renda`) REFERENCES `tb_imposto_renda`(`id_imposto_renda`);

ALTER TABLE `tb_folha_pagamento` ADD CONSTRAINT `tb_folha_pagamento_fk3` FOREIGN KEY (`id_sal_familia`) REFERENCES `tb_salario_familia`(`id_sal_familia`);

ALTER TABLE `tb_imposto_renda` ADD CONSTRAINT `tb_imposto_renda_fk0` FOREIGN KEY (`id_folha_pag`) REFERENCES `tb_folha_pagamento`(`id_folha_pag`);
